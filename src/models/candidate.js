var shortid = require('shortid');

module.exports = {
  identity: 'candidate',
  connection: 'pizzaDB',
  attributes: {
    id: {
      type: 'string',
      primaryKey: true,
      unique: true,
      defaultsTo: function() {
        return shortid.generate();
      }
    },
    name: {
      type : 'string',
      required : true
    }
  }
};

// module.exports = function add(req, reply) {
//     var candidateData = {name: 'Zeeshan'}
//     candidate.create(candidateData).exec(function(err, result) {
//         if(err) {
//             return console.log('Not added');
//         }
//         return reply('Success');
//     })
// }