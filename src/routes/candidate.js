module.exports = [{
  // return all candidate items
  path: '/candidate',
  method: 'GET',
  handler: function(req, reply) {
      const candidate = req.model.candidate;
      req.model.candidate.find().exec(function(err, records) {
          if(err) {
              console.log('error find all----', err);
          }
          reply(records);
      });
  }
}, {
  // return a specific candidate by id
  path: '/candidate/{id}',
  method: 'GET',
  handler: function(req, reply) {
      req.model.candidate.findOne({id : req.params.id}).exec(function(err, records) {
          if(err) {
              return console.log('error candidate id--', err);
          }
          console.log(records);
          reply(records)
      });
  }
}, {
  // create a new candidate
  path: '/candidate',
  method: 'POST',
  handler: function(req, reply) {
      req.model.candidate.create(req.payload).exec(function(err, result) {
          if(err) {
              return console.log('create error----', err);
          }
          reply('candidate created')
      });
  }
}, {
  // udpate an existing candidate by id
  path: '/candidate/{id}',
  method: ['PATCH', 'POST'],
  handler: function(req, reply) {
      req.model.candidate.update({id: req.params.id}, {name: req.payload.name})
        .exec(function(err, result) {
            if(err) {
                return console.log('update error-----', err);
            }
            reply('updated');
        });
  }
}, {
  // remove a candidate by id
  path: '/candidate/{id}',
  method: 'DELETE',
  handler: function(req, reply) {
      console.log(req.params);
      req.model.candidate.destroy({id: req.params.id})
        .exec(function(err, result) {
            if(err) {
                return console.log('update error-----', err);
            }
            reply('deleted');
        });
  }
}];


